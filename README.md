# ShareBook Android App.

Share a book, share culture.


## Description

This a RESTfull Web/Android app based on: 

- Java
- Gradle
- Android Studio
- REST
- MySQL


### Pitch Presentation.

Please find the PDF pitch presentation for this app within the repository. 

For the web version follow: 

https://gitlab.com/diegomendez31/SpringBootShareApp
